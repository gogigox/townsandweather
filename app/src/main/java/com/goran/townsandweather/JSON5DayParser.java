package com.goran.townsandweather;

import com.goran.townsandweather.net.model.Day5ModelActivity;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JSON5DayParser {

    //klasa potrebna da bi smo mogli da preuzimamo i parsiramo JSON podatke sa neta
    //povezana sa Day5ModelActivity
    public static Day5ModelActivity getDay5ModelActivity(String data) throws JSONException {
        if (data != null) {

            Day5ModelActivity day5ModelActivity = new Day5ModelActivity();

            //kreiramo JSON objekt iz podataka
            JSONObject jObj = new JSONObject(data);

            day5ModelActivity.setCod(getString("cod", jObj));
            day5ModelActivity.setMessage(getInt("message", jObj));
            day5ModelActivity.setCnt(getInt("cnt", jObj));

            //ovde je lista JSON objekata i  dalje je sve pracenje gde je lista a gde objekti
            JSONArray weatherArrayList = jObj.getJSONArray("list");
            List<Day5ModelActivity.List> day5List = new ArrayList<>();
            for (int i = 0; i < weatherArrayList.length(); i++) {

                Day5ModelActivity.List el = new Day5ModelActivity.List();
                JSONObject JSONObList = weatherArrayList.getJSONObject(i);
                el.setDt(getDouble("dt", JSONObList)*1000);
                Day5ModelActivity.List.Main main = new Day5ModelActivity.List.Main();
                JSONObject mainObj = getObject("main", JSONObList);
                main.setTemp(getFloat("temp", mainObj));
                main.setTempMin(getFloat("temp_min", mainObj));
                main.setTempMax(getFloat("temp_max", mainObj));
                main.setPressure(getInt("pressure", mainObj));
                main.setSeaLevel(getInt("sea_level", mainObj));
                main.setGrndLevel(getInt("grnd_level", mainObj));
                main.setHumidity(getInt("humidity", mainObj));
                main.setTempKf(getFloat("temp_kf", mainObj));
                el.setMain(main);

                JSONArray jArr = JSONObList.getJSONArray("weather");
                JSONObject JSONWeather = jArr.getJSONObject(0);
                Day5ModelActivity.List.Weather weather = new Day5ModelActivity.List.Weather();
                weather.setId(getInt("id", JSONWeather));
                weather.setMain(getString("main", JSONWeather));
                weather.setDescription(getString("description", JSONWeather));
                weather.setIcon(getString("icon", JSONWeather));
                ArrayList<Day5ModelActivity.List.Weather> listWeather = new ArrayList<>();
                listWeather.add(weather);
                el.setWeather(listWeather);

                JSONObject JSONClouds = getObject("clouds", JSONObList);
                Day5ModelActivity.Clouds clouds = new Day5ModelActivity.Clouds();
                clouds.setAll(getInt("all", JSONClouds));
                el.setClouds(clouds);

                JSONObject JSONWind = getObject("wind", JSONObList);
                Day5ModelActivity.List.Wind wind = new Day5ModelActivity.List.Wind();
                wind.setSpeed(getFloat("speed", JSONWind));
                wind.setDeg(getInt("deg", JSONWind));
                el.setWind(wind);

                //za snow treba if jer su stavili da ga nekad ima a nekad ne
                if(JSONObList.has("snow")){
                    JSONObject JSONSnow = getObject("snow", JSONObList);
                    Day5ModelActivity.List.Snow snow = new Day5ModelActivity.List.Snow();
                    snow.set_3h(getFloat("3h", JSONSnow));
                    el.setSnow(snow);
                }

                JSONObject JSONSys = getObject("sys", JSONObList);
                Day5ModelActivity.List.Sys sys = new Day5ModelActivity.List.Sys();
                sys.setPod(getString("pod", JSONSys));
                el.setSys(sys);

                el.setDtTxt(getString("dt_txt", JSONObList));

                day5List.add(el);

            }

            day5ModelActivity.setList(day5List);

            JSONObject JSONCity = getObject("city", jObj);
            Day5ModelActivity.City city = new Day5ModelActivity.City();
            city.setId(getInt("id", JSONCity));
            city.setName(getString("name", JSONCity));

            JSONObject JSONCoord = getObject("coord", JSONCity);
            Day5ModelActivity.Coord coord = new Day5ModelActivity.Coord();
            coord.setLat(getFloat("lat", JSONCoord));
            coord.setLon(getFloat("lon", JSONCoord));
            city.setCoord(coord);
            city.setCountry(getString("country", JSONCity));

            day5ModelActivity.setCity(city);


            return day5ModelActivity;

        }
        return null;
    }


    private static JSONObject getObject(String tagName, JSONObject jObj) throws JSONException {
        JSONObject subObj = jObj.getJSONObject(tagName);
        return subObj;
    }

    private static String getString(String tagName, JSONObject jObj) throws JSONException {
        return jObj.getString(tagName);
    }

    private static float getFloat(String tagName, JSONObject jObj) throws JSONException {
        return (float) jObj.getDouble(tagName);
    }

    private static int getInt(String tagName, JSONObject jObj) throws JSONException {
        return jObj.getInt(tagName);
    }

    private static double getDouble(String tagName, JSONObject jObj) throws JSONException {
        return jObj.getDouble(tagName);
    }

}