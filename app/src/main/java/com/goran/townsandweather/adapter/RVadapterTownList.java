package com.goran.townsandweather.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.goran.townsandweather.JSONWeatherTask;
import com.goran.townsandweather.R;
import com.goran.townsandweather.db.model.Town;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.ExecutionException;


public class RVadapterTownList extends RecyclerView.Adapter<RVadapterTownList.MyViewHolder> {


    private List<Town> townList;

    public RVadapterTownList.OnRVItemClick listenerTownList;

    public interface OnRVItemClick {
        void onRVItemclick(Town town);

    }


    public RVadapterTownList(List<Town> townList, OnRVItemClick listenerTownList) {
        this.townList = townList;
        this.listenerTownList = listenerTownList;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {


        TextView tvTownWrittenName;
        TextView tvWeatherWritten;
        TextView tvTownName;
        TextView tvWeather;
        ImageView rvImage;
        View view;


        public MyViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tvTownWrittenName = itemView.findViewById(R.id.rv_town_written_name);
            tvWeatherWritten = itemView.findViewById(R.id.rv_town_written_weather);
            tvTownName = itemView.findViewById(R.id.rv_town_name);
            tvWeather = itemView.findViewById(R.id.rv_town_weather);
            rvImage = itemView.findViewById(R.id.rv_image);

        }


        public void bind(final Town town, final RVadapterTownList.OnRVItemClick listener) {


            tvTownName.setText(town.getName());

            String city = town.getName();

            JSONWeatherTask task = new JSONWeatherTask();
            task.execute(new String[]{city});

            try {
//detaljnije objasnjenjene za ovaj red je u Day5Activity 166 red-pretvaranje kelvina u celzijuse i zaokruzivanje
                tvWeather.setText(Math.round(task.get().temperature.getTemp() - 273.15) + " °C");

                String icon = task.get().currentCondition.getIcon();
                String iconUrl = "http://openweathermap.org/img/w/" + icon + ".png";
                Picasso.get().load(iconUrl).into((ImageView) rvImage.findViewById(R.id.rv_image));


            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRVItemclick(town);
                }
            });
        }

    }


    @NonNull
    @Override
    public RVadapterTownList.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.rv_town_single_item, viewGroup, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RVadapterTownList.MyViewHolder myViewHolder, int i) {

        myViewHolder.bind(townList.get(i), listenerTownList);
    }


    @Override
    public int getItemCount() {
        return townList.size();
    }

}