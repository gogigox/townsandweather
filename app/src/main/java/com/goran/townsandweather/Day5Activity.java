package com.goran.townsandweather;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.goran.townsandweather.net.model.Day5ModelActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.ExecutionException;


//klasa koja prikazuje prognozu za 5 dana
public class Day5Activity extends AppCompatActivity {


    private EditText etDay1;
    private EditText etDay2;
    private EditText etDay3;
    private EditText etDay4;
    private EditText etDay5;
    private ImageView iv1;
    private ImageView iv2;
    private ImageView iv3;
    private ImageView iv4;
    private ImageView iv5;
    private EditText etTemp1;
    private EditText etTemp2;
    private EditText etTemp3;
    private EditText etTemp4;
    private EditText etTemp5;

    private FloatingActionButton fabBack;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day5);

        etDay1 = findViewById(R.id.et_day1);
        etDay2 = findViewById(R.id.et_day2);
        etDay3 = findViewById(R.id.et_day3);
        etDay4 = findViewById(R.id.et_day4);
        etDay5 = findViewById(R.id.et_day5);
        iv1 = findViewById(R.id.iv1);
        iv2 = findViewById(R.id.iv2);
        iv3 = findViewById(R.id.iv3);
        iv4 = findViewById(R.id.iv4);
        iv5 = findViewById(R.id.iv5);
        etTemp1 = findViewById(R.id.et_temp1);
        etTemp2 = findViewById(R.id.et_temp2);
        etTemp3 = findViewById(R.id.et_temp3);
        etTemp4 = findViewById(R.id.et_temp4);
        etTemp5 = findViewById(R.id.et_temp5);

        fabBack = findViewById(R.id.day5_activity_fab_back);

        etDay1.setFocusable(false);
        etDay2.setFocusable(false);
        etDay3.setFocusable(false);
        etDay4.setFocusable(false);
        etDay5.setFocusable(false);
        etTemp1.setFocusable(false);
        etTemp2.setFocusable(false);
        etTemp3.setFocusable(false);
        etTemp4.setFocusable(false);
        etTemp5.setFocusable(false);


        showWeather();

        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Day5Activity.this, MainActivity.class);
                startActivity(intent);

            }
        });

    }

//metoda koja se poziva da prikaza prognozu za 5 dana
    private void showWeather() {

        //iz MainActivity ili Favorites Activity prosledjujemo string city koji je naziv konkretnog grada
        //ciju prognozu za 5 dana ispisuje
        String city = getIntent().getStringExtra("CITY");

        //pokrecemo novi JSONWeatherTask
        JSONWeatherTask task = new JSONWeatherTask();
        task.execute(new String[]{city});

        //napravili smo ArrayList koja sadrzi elemente tipa Day5ModelActivity.List i nazvali je list5Days
        //ovo smo uradili da bismo u list5Days stavili one clanove tipa Day5ModelActivity.List koji nama trebaju
        //svaki element koji je tipa Day5ModelActivity.List sadrzi prognozu koja je radjena na svaka 3h
        //nama trebaju podaci koji se odnose na vremenske prilike u samo u 12h u narednih 5 dana, znaci ima
        // dosta podataka koji nam ne trebaju
        //dole prolazimo kroz foreach petlju, pojedinacni element tipa Day5ModelActivity.List smo nazvali day
        //task.get() nam je od private class JSONWeatherTask(dole je) i vraca Day5ModelActivity i sa getList()
        //prolazimo kroz celu listu Day5ModelActivity.List
        ArrayList<Day5ModelActivity.List> list5Days = new ArrayList<>();

        try {
            for (Day5ModelActivity.List day : task.get().getList()) {

                 //day je pojedinacni element gorepomenute liste a day.getDtTxt() je vreme(merenja su na svaka 3h)
                //a nama treba samo u podne, tako da uzimamo samo one elemente kojima je vreme(DtTxt) 12
                //i njih stavljamo u nasu listu list5Days(bice ih 5 jer je prognoza za 5 dana)
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                cal.setTime(sdf.parse(day.getDtTxt()));

                if (cal.get(Calendar.HOUR_OF_DAY) == 12) {

                    list5Days.add(day);

                }

            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }


         // for ide od 1 a ne od 0 jer je prvi et_day1 a ne et_day0 (tu se gledaju id iz xml a ne etDay koji smo
        // napravili u ovoj klasi

        for (int i = 1; i <= 5; i++) {

            //editTextDay ispisuje dane
            int id = getResources().getIdentifier("et_day" + i, "id", getApplicationContext().getPackageName());
            EditText editTextDay = findViewById(id);

            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {

                //parsiramo vreme svakog clana list5Days a ide i-1 jer je ovde prvi element na 0 mestu
                cal.setTime(sdf.parse(list5Days.get(i - 1).getDtTxt()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            editTextDay.setText(cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.ENGLISH));


            //stavlja se slicica, a ide getWeather().get(0).getIcon() jer je i weather lista, ali ima samo jedan - nulti el
            //koji sadrzi id, main, description i icon
            int id2 = getResources().getIdentifier("iv" + i, "id", getApplicationContext().getPackageName());

            String icon = list5Days.get(i - 1).getWeather().get(0).getIcon();
            String iconUrl = "http://openweathermap.org/img/w/" + icon + ".png";
            Picasso.get().load(iconUrl).into((ImageView) findViewById(id2));


            //stavlja se temp, dole je prikazano pretvaranje kelvina u celzijuse i zaoukruzivanje dobijenog broja
            //to sve moze i da se stavi u 1 red
            int id3 = getResources().getIdentifier("et_temp" + i, "id", getApplicationContext().getPackageName());
            EditText editTextTemp = findViewById(id3);

//            float kelvin = list5Days.get(i - 1).getMain().getTemp();
//            double celsius = kelvin - 273.15;
//            double celsiusuRound = Math.round(celsius);
            editTextTemp.setText(Math.round(list5Days.get(i - 1).getMain().getTemp() - 273.15) + "  °C");


        }

    }


    //klasa koja nam daje task.get(), izvsava se u pozadini, parametri su <String, Void, Day5ModelActivity>
    //za razliku od istoimene klase u ovom projektu koja je public i ciji su parametri <String, Void, Weather>
    private class JSONWeatherTask extends AsyncTask<String, Void, Day5ModelActivity> {


        @Override
        protected Day5ModelActivity doInBackground(String... params) {
            Day5ModelActivity day5ModelActivity = new Day5ModelActivity();
            String data = ((new WeatherHttpClient()).getDay5Data(params[0]));

            try {
                day5ModelActivity = JSON5DayParser.getDay5ModelActivity(data);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return day5ModelActivity;

        }

    }

}
