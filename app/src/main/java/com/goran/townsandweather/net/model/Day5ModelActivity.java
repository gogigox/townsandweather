package com.goran.townsandweather.net.model;


//model klasa preuzeta sa neta, tu su svi podaci koje sajt openweatherapi nudi
//ovde je rec o prognozi za 5 dana
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



public class Day5ModelActivity {



    @SerializedName("cod")
    @Expose
    private String cod;
    @SerializedName("message")
    @Expose
    private Integer message;
    @SerializedName("cnt")
    @Expose
    private Integer cnt;
    @SerializedName("list")
    @Expose
    private java.util.List<Day5ModelActivity.List> list = null;
    @SerializedName("city")
    @Expose
    private City city;


    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Integer getMessage() {
        return message;
    }

    public void setMessage(Integer message) {
        this.message = message;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }


    public static class City {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("coord")
        @Expose
        private Coord coord;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("timezone")
        @Expose
        private Integer timezone;
        @SerializedName("sunrise")
        @Expose
        private Integer sunrise;
        @SerializedName("sunset")
        @Expose
        private Integer sunset;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Coord getCoord() {
            return coord;
        }

        public void setCoord(Coord coord) {
            this.coord = coord;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public Integer getTimezone() {
            return timezone;
        }

        public void setTimezone(Integer timezone) {
            this.timezone = timezone;
        }

        public Integer getSunrise() {
            return sunrise;
        }

        public void setSunrise(Integer sunrise) {
            this.sunrise = sunrise;
        }

        public Integer getSunset() {
            return sunset;
        }

        public void setSunset(Integer sunset) {
            this.sunset = sunset;
        }
    }


    public static class Clouds {

        @SerializedName("all")
        @Expose
        private Integer all;

        public Integer getAll() {
            return all;
        }

        public void setAll(Integer all) {
            this.all = all;
        }
    }


    public static class Coord {

        @SerializedName("lat")
        @Expose
        private Float lat;
        @SerializedName("lon")
        @Expose
        private Float lon;

        public Float getLat() {
            return lat;
        }

        public void setLat(Float lat) {
            this.lat = lat;
        }

        public Float getLon() {
            return lon;
        }

        public void setLon(Float lon) {
            this.lon = lon;
        }
    }


    public static class List {


        @SerializedName("dt")
        @Expose
        private Double dt;
        @SerializedName("main")
        @Expose
        private Main main;
        @SerializedName("weather")
        @Expose
        private java.util.List<Weather> weather = null;
        @SerializedName("clouds")
        @Expose
        private Clouds clouds;
        @SerializedName("wind")
        @Expose
        private Wind wind;
        @SerializedName("snow")
        @Expose
        private Snow snow;
        @SerializedName("sys")
        @Expose
        private Sys sys;
        @SerializedName("dt_txt")
        @Expose
        private String dtTxt;


        public Double getDt() {
            return dt;
        }

        public void setDt(Double dt) {
            this.dt = dt;
        }

        public Main getMain() {
            return main;
        }

        public void setMain(Main main) {
            this.main = main;
        }

        public java.util.List<Weather> getWeather() {
            return weather;
        }

        public void setWeather(java.util.List<Weather> weather) {
            this.weather = weather;
        }

        public Clouds getClouds() {
            return clouds;
        }

        public void setClouds(Clouds clouds) {
            this.clouds = clouds;
        }

        public Wind getWind() {
            return wind;
        }

        public void setWind(Wind wind) {
            this.wind = wind;
        }

        public Sys getSys() {
            return sys;
        }

        public void setSys(Sys sys) {
            this.sys = sys;
        }

        public String getDtTxt() {
            return dtTxt;
        }

        public void setDtTxt(String dtTxt) {
            this.dtTxt = dtTxt;
        }

        public Snow getSnow() {
            return snow;
        }

        public void setSnow(Snow snow) {
            this.snow = snow;
        }

        public static class Main {

            @SerializedName("temp")
            @Expose
            private Float temp;
            @SerializedName("feels_like")
            @Expose
            private Float feelsLike;
            @SerializedName("temp_min")
            @Expose
            private Float tempMin;
            @SerializedName("temp_max")
            @Expose
            private Float tempMax;
            @SerializedName("pressure")
            @Expose
            private Integer pressure;
            @SerializedName("sea_level")
            @Expose
            private Integer seaLevel;
            @SerializedName("grnd_level")
            @Expose
            private Integer grndLevel;
            @SerializedName("humidity")
            @Expose
            private Integer humidity;
            @SerializedName("temp_kf")
            @Expose
            private Float tempKf;

            public Float getTemp() {
                return temp;
            }

            public void setTemp(Float temp) {
                this.temp = temp;
            }

            public Float getFeelsLike() {
                return feelsLike;
            }

            public void setFeelsLike(Float feelsLike) {
                this.feelsLike = feelsLike;
            }

            public Float getTempMin() {
                return tempMin;
            }

            public void setTempMin(Float tempMin) {
                this.tempMin = tempMin;
            }

            public Float getTempMax() {
                return tempMax;
            }

            public void setTempMax(Float tempMax) {
                this.tempMax = tempMax;
            }

            public Integer getPressure() {
                return pressure;
            }

            public void setPressure(Integer pressure) {
                this.pressure = pressure;
            }

            public Integer getSeaLevel() {
                return seaLevel;
            }

            public void setSeaLevel(Integer seaLevel) {
                this.seaLevel = seaLevel;
            }

            public Integer getGrndLevel() {
                return grndLevel;
            }

            public void setGrndLevel(Integer grndLevel) {
                this.grndLevel = grndLevel;
            }

            public Integer getHumidity() {
                return humidity;
            }

            public void setHumidity(Integer humidity) {
                this.humidity = humidity;
            }

            public Float getTempKf() {
                return tempKf;
            }

            public void setTempKf(Float tempKf) {
                this.tempKf = tempKf;
            }
        }


        public static class Sys {

            @SerializedName("pod")
            @Expose
            private String pod;

            public String getPod() {
                return pod;
            }

            public void setPod(String pod) {
                this.pod = pod;
            }
        }


        public static class Weather {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("main")
            @Expose
            private String main;
            @SerializedName("description")
            @Expose
            private String description;
            @SerializedName("icon")
            @Expose
            private String icon;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getMain() {
                return main;
            }

            public void setMain(String main) {
                this.main = main;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getIcon() {
                return icon;
            }

            public void setIcon(String icon) {
                this.icon = icon;
            }
        }


        public static class Wind {

            @SerializedName("speed")
            @Expose
            private Float speed;
            @SerializedName("deg")
            @Expose
            private Integer deg;

            public Float getSpeed() {
                return speed;
            }

            public void setSpeed(Float speed) {
                this.speed = speed;
            }

            public Integer getDeg() {
                return deg;
            }

            public void setDeg(Integer deg) {
                this.deg = deg;
            }
        }

        public static class Snow {
            @SerializedName("3h")
            @Expose
            private double _3h;

            public double get_3h() {
                return _3h;
            }

            public void set_3h(double _3h) {
                this._3h = _3h;
            }
        }
    }


}
