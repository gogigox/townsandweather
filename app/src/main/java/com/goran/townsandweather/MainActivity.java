package com.goran.townsandweather;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Looper;
import android.provider.Settings;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.goran.townsandweather.db.DatabaseHelper;
import com.goran.townsandweather.db.model.Town;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.squareup.picasso.Picasso;


import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;



public class MainActivity extends AppCompatActivity {

    private Town town;

    private Toolbar toolbar;
    private EditText etSearchText;
    private Button searchBtn;
    private EditText etTownWeather;
    private ImageView ivPicture;
    private EditText etTownTemp;
    private Button day5Btn;
    private Button addBtn;
    private EditText etYourLocation;
    private EditText etLocalWeather;
    private ImageView ivPictureLocal;
    private EditText etLocalTemp;
    private Button goBtn;

    private int btn;

    private DatabaseHelper databaseHelper;

    private int PERMISSION_ID = 44;
    private FusedLocationProviderClient mFusedLocationClient;
    private double lat;
    private double lon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        etSearchText = findViewById(R.id.searchText);
        searchBtn = findViewById(R.id.searchBtn);
        etTownWeather = findViewById(R.id.et_town_weather);
        etTownWeather.setFocusable(false);
        ivPicture = findViewById(R.id.imageView2);
        etTownTemp = findViewById(R.id.et_town_temp);
        etTownTemp.setFocusable(false);
        day5Btn = findViewById(R.id.day5Btn);
        addBtn = findViewById(R.id.addBtn);
        etYourLocation = findViewById(R.id.et_your_location);
        etYourLocation.setFocusable(false);
        etLocalWeather = findViewById(R.id.et_local_weather);
        etLocalWeather.setFocusable(false);
        ivPictureLocal = findViewById(R.id.imageLocal);
        etLocalTemp = findViewById(R.id.et_local_temp);
        etLocalTemp.setFocusable(false);
        goBtn = findViewById(R.id.goBtn);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        getLastLocation();


           setupToolbar();

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btn = 1;
                search();

            }
        });

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btn = 2;
                search();

            }
        });

        goBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, FavoritesActivity.class);
                startActivity(intent);


            }
        });

        day5Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btn = 3;
                search();

            }
        });


    }


    private void search() {

        String city = etSearchText.getText().toString();

        //startujemo novi JSONWeatherTask, prikazuje nam trenutno vreme grada koji je upisao korisnik
        JSONWeatherTask task = new JSONWeatherTask();
        task.execute(new String[]{city});

        try {

            //task.get() nam vraca Weather, pa ako je Weather null, znaci da grad ne postoji
            //u bazi podataka kojom raspolaze openweather sajt
            if (task.get() != null) {

                //ako grad postoji onda sa btn 1 prikazuje vreme, sa brn 2 dodaje u listu omiljenih
                //a as btn 3 prikazuje prognozu za 5 dana
                if (btn == 1) {
                    etTownWeather.setText(task.get().currentCondition.getCondition());

                    String icon = task.get().currentCondition.getIcon();
                    String iconUrl = "http://openweathermap.org/img/w/" + icon + ".png";
                    Picasso.get().load(iconUrl).into((ImageView) findViewById(R.id.imageView2));

                    //detaljnije objasnjenjene za ovaj red je u Day5Activity 175 red-pretvaranje kelvina u celzijuse i zaokruzivanje
                    etTownTemp.setText(Math.round(task.get().temperature.getTemp() - 273.15) + "  °C");

                } else if (btn == 2) {
                    add();

                }  else if (btn == 3) {
                    open5DaysForecast();
                }
            } else {
                Toast.makeText(MainActivity.this, "Nonexistent town!", Toast.LENGTH_SHORT).show();
            }
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

    }



    // Pocetak setovanja toolbara
    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.show();
        }
    }


    private void add() {

        town = new Town();
        town.setName(etSearchText.getText().toString());

        try {
            getDatabaseHelper().getTownDao().create(town);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Toast.makeText(MainActivity.this, "Town added", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(MainActivity.this, FavoritesActivity.class);
        startActivity(intent);

    }


    private void open5DaysForecast(){

        String city = etSearchText.getText().toString();

        Intent intent = new Intent(MainActivity.this, Day5Activity.class);
        intent.putExtra("CITY", city);
        startActivity(intent);
    }



    //dalji deo koda se odnosi na trenutnu lokaciju korisnika - tu su permisije koje su potrebne da budu odobrene
    //GPS i ispisivanje tih podataka
    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }


    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_ID
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            }
        }
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }

    @SuppressLint("MissingPermission")
    private void getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {
                                    lat = location.getLatitude();
                                    lon = location.getLongitude();
                                    String city = getCity(lat, lon);
                                    if (city != null) {
                                        EditText et = findViewById(R.id.et_your_location);
                                        et.setText(city);

                                        JSONWeatherTask task1 = new JSONWeatherTask();
                                        task1.execute(new String[]{city});

                                        try {

                                            etLocalWeather.setText(task1.get().currentCondition.getCondition());

                                            String icon = task1.get().currentCondition.getIcon();
                                            String iconUrl = "http://openweathermap.org/img/w/" + icon + ".png";
                                            Picasso.get().load(iconUrl).into((ImageView) findViewById(R.id.imageLocal));

                                            //detaljnije objasnjenjene za ovaj red je u Day5Activity 166 red-pretvaranje
                                            // kelvina u celzijuse i zaokruzivanje
                                            etLocalTemp.setText(Math.round(task1.get().temperature.getTemp() - 273.15) + "  °C");

                                        } catch (ExecutionException e) {
                                            e.printStackTrace();
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }

                                    } else {
                                            Toast.makeText(getApplicationContext(), "Location not found!", Toast.LENGTH_LONG).show();
                                    }


                                }
                            }
                        }
                );
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }

    @SuppressLint("MissingPermission")
    private void requestNewLocationData() {

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }

    //ovo je nesto za automatsko osvezavanje prognoze
// https://www.androdocs.com/java/getting-current-location-latitude-longitude-in-android-using-java.html
    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();

        }
    };

    private String getCity(double lat, double lon) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(lat, lon, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String cityName = null;
        if (addresses.size() != 0) {
            cityName = addresses.get(0).getLocality();
        }
        return cityName;
    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

// nakon rada sa bazo podataka potrebno je obavezno
//osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

}