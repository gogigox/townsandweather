package com.goran.townsandweather;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.goran.townsandweather.adapter.RVadapterTownList;
import com.goran.townsandweather.db.DatabaseHelper;
import com.goran.townsandweather.db.model.Town;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class FavoritesActivity extends AppCompatActivity implements RVadapterTownList.OnRVItemClick {


    private RecyclerView recyclerView;

    private RVadapterTownList rVadapterTownList;

    private Toolbar toolbar;
    private FloatingActionButton fabBack;

    private DatabaseHelper databaseHelper;

    private List<Town> townList = new ArrayList<>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);


        fabBack = findViewById(R.id.fav_activity_fab_back);


        setupToolbar();
        setupRV();


        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(FavoritesActivity.this, MainActivity.class);
                startActivity(intent);

            }
        });


    }


    private void setupRV() {
        recyclerView = findViewById(R.id.rv_town_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }


    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }


    @Override
    protected void onResume() {
        super.onResume();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchHelperCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
        if (rVadapterTownList == null) {

            try {
                townList = getDatabaseHelper().getTownDao().queryForAll();
                rVadapterTownList = new RVadapterTownList(townList, this);
                recyclerView.setAdapter(rVadapterTownList);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            rVadapterTownList.notifyDataSetChanged();
        }
    }


    @Override
    public void onRVItemclick(Town town) {

        String city = town.getName();

        //kad kliknemo na element rv liste otvara nam Day5Activity- prognozu za 5 dana za odredjeni grad
        //zato trba  intent.putExtra i prosledjujemo ime grada
        Intent intent = new Intent(FavoritesActivity.this, Day5Activity.class);
        intent.putExtra("CITY", city);
        startActivity(intent);

    }


    //implementiran SimpleCallback koji omogucava da se pokretom u levo izbrise grad iz liste
    ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder,
                              @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
            try {
                Town town = getDatabaseHelper().getTownDao().queryForId(townList.get(viewHolder.getLayoutPosition()).getId());
                getDatabaseHelper().getTownDao().delete(town);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            townList.remove(viewHolder.getLayoutPosition());
            rVadapterTownList.notifyItemRemoved(viewHolder.getLayoutPosition());

        }
    };


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

// nakon rada sa bazo podataka potrebno je obavezno
//osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }


}