package com.goran.townsandweather.db.model;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


// nasa model klasa (nije preuzeta sa neta), dovoljno je da ima samo id i ime, trazicemo vreme u odredjenom gradu i
// pravicemo listu omiljenih gradova
@DatabaseTable(tableName = "town")
public class Town {

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(columnName = "name", unique = true)
    private String name;


    public Town() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Town{" +
                "name='" + name + '\'' +
                '}';
    }
}
