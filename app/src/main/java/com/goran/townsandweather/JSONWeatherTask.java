package com.goran.townsandweather;

import android.os.AsyncTask;

import com.goran.townsandweather.net.model.Weather;

import org.json.JSONException;


public class JSONWeatherTask extends AsyncTask<String, Void, Weather> {

    @Override
    protected Weather doInBackground(String... params) {
        Weather weather = new Weather();
        String data = ((new WeatherHttpClient()).getWeatherData(params[0]));

        try {
            weather = JSONWeatherParser.getWeather(data);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return weather;

    }
}
